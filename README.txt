Webform Sign PDF Example

Optional third party library installation instructions (recommended)

If using redhat, you'd add this to your Dockerfile (for the optional wkhtmltopdf library):

# Begin dependency install for PDF library wkhtmltox /wkhtmltopdf.
RUN  yum install -y http://repo.okay.com.mx/centos/8/x86_64/release/xorg-x11-fonts-75dpi-7.5-19.el8.noarch.rpm
#### wkhtmltox is the RHEL 8 version of wxhtmltopdf library that enhances entity_print (to pdf).
RUN wget https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox-0.12.6-1.centos8.x86_64.rpm && \
    dnf localinstall wkhtmltox-0.12.6-1.centos8.x86_64.rpm -y && \
    yum clean all

Otherwise, if you're using one of the more popular systems like Ubuntu/Debian then have a look here:

https://docs.bitnami.com/installer/apps/odoo/configuration/install-wkhtmltopdf/

Installation of this module:

surefire way: 
composer require drupal/webform_sign_pdf_example -w

This is a heavy hitting one though because it'll upgrade your other dependencies.
